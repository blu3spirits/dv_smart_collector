# TODO: API Exceptions

import requests


class DiskViewerApi(object):
    def __init__(self, logger, api_config):
        self.logger = logger
        self.api_config = api_config
        self.API_URL = api_config['url']

    def post_disk(self, disk_data):
        self.logger.info(f'Posting new disk with data {disk_data}')
        resp = requests.post(f'{self.API_URL}/disks/', json=disk_data)
        if resp.ok:
            results = resp.json()
            return results
        else:
            # TODO This will probably except
            print(resp.text)
            print('someting wong')

    def update_disk(self, disk_id, disk_data):
        self.logger.info(f'Updating disk {disk_id} with {disk_data}')
        resp = requests.patch(f'{self.API_URL}/disks/{disk_id}/', json=disk_data)
        if resp.ok:
            results = resp.json()
            return results
        else:
            # TODO This will probably except
            print(resp.text)
            print('someting wong')

    def create_server(self, server_data):
        self.logger.info(f'Posting new server with data {server_data}')
        resp = requests.post(f'{self.API_URL}/servers/', json=server_data)
        if resp.ok:
            results = resp.json()
            return results
        else:
            # TODO: Exception
            print(resp.text)
            print(f'something wrong with creating server with data {server_data}')

    def get_servers(self, hostname):
        resp = requests.get(f'{self.API_URL}/servers/?hostname={hostname}')
        if resp.ok:
            results = resp.json()
            if results['count'] == 1:
                return results['results'][0]
            elif results['count'] == 0:
                return None
            else:
                # TODO: Exception
                print(resp.text)
                print('server returned more than 1 result?')
        else:
            # TODO: Exception
            print(resp.text)
            print('someting wong')

    def get_disks(self, disk=None):
        if disk:
            resp = requests.get(f'{self.API_URL}/disks/?serial_number={disk}')
        else:
            resp = requests.get(f'{self.API_URL}/disks/')

        if resp.ok:
            results = resp.json()
            if results['results']:
                return results
            else:
                # TODO: Except
                print(resp.text)
                print('no disks found!')

        else:
            # TODO: Add Exceptions/Logging here
            print(resp.text)
            print('someting wong')
