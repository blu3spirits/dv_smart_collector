import logging
import sys
import inspect
from pathlib import Path
from logging.handlers import RotatingFileHandler


class CollectorLogger(object):
    def __init__(self, log_dir: str, log_file: str, log_level=logging.INFO):
        self.logger = logging.getLogger()
        if log_level:
            if log_level == 'DEBUG':
                log_level = logging.DEBUG
            elif log_level == 'INFO':
                log_level = logging.INFO
            elif log_level == 'WARNING':
                log_level = logging.WARNING
            elif log_level == 'ERROR':
                log_level = logging.ERROR
            elif log_level == 'CRITICAL':
                log_level = logging.CRITICAL
        self.logger.setLevel(log_level)
        log_format = logging.Formatter(
            fmt=("[%(asctime)s][%(levelname)s][%(name)s] "
                 "%(message)s"),
            # Nearly ISO 8601 compliant, strftime doesn't support nanoseconds?
            # https://docs.python.org/3/library/time.html#time.strftime
            datefmt="%Y-%m-%dT%H:%M:%S"
        )
        stdio_handler = logging.StreamHandler(sys.stdout)

        if log_file and log_dir:
            if not Path(log_dir).exists():
                Path(log_dir).mkdir(parents=True, exist_ok=True)

        rotating_handler = RotatingFileHandler(log_file,
                                               maxBytes=1024**3)
        rotating_handler.setFormatter(log_format)
        self.logger.addHandler(rotating_handler)
        self.logger.addHandler(stdio_handler)

    def debug(self, msg):
        """
        Wrapper for logger.debug()
        """
        name = f'{inspect.stack()[1][1]}][{inspect.stack()[1][3]}'
        self.logger = logging.getLogger(name)
        self.logger.debug(msg)

    def error(self, msg):
        """
        Wrapper for logger.error()
        """
        name = f'{inspect.stack()[1][1]}][{inspect.stack()[1][3]}'
        self.logger = logging.getLogger(name)
        self.logger.error(msg)

    def info(self, msg):
        """
        Wrapper for logger.info()
        """
        name = f'{inspect.stack()[1][1]}][{inspect.stack()[1][3]}'
        self.logger = logging.getLogger(name)
        self.logger.info(msg)

    def warning(self, msg):
        """
        Wrapper for logger.warning()
        """
        name = f'{inspect.stack()[1][1]}][{inspect.stack()[1][3]}'
        self.logger = logging.getLogger(name)
        self.logger.warning(msg)

    def critical(self, msg):
        """
        Wrapper for logger.critical()
        """
        name = f'{inspect.stack()[1][1]}][{inspect.stack()[1][3]}'
        self.logger = logging.getLogger(name)
        self.logger.critical(msg)
