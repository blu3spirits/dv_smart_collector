class SmartCollectorException(Exception):
    def __init__(self, message, fatal=False):
        self.message = message
        self.fatal = fatal
        Exception.__init__(self, self.message, self.fatal)

    def __str__(self):
        return str(self.message)


class SmartCollectorSmartctlError(SmartCollectorException):
    def __init__(self, message, fatal=False):
        self.message = message
        self.fatal = fatal


class SmartCollectorKeyError(SmartCollectorException):
    def __init__(self, message, fatal=False):
        self.message = message
        self.fatal = fatal

class SmartCollectorSmartctlNoTableError(SmartCollectorException):
    def __init__(self, message, fatal=False):
        self.message = message
        self.fatal = fatal


class SmartCollectorApiPostException(SmartCollectorException):
    ...

class SmartCollectorApiGetException(SmartCollectorException):
    ...

class SmartCollectorApiPatchException(SmartCollectorException):
    ...

class SmartCollectorApiPostException(SmartCollectorException):
    ...
