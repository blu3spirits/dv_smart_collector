from configurator import Config


class CollectorConfig:
    system_config = Config({
        'command': {
            'smart_command': 'smartctl --all --json',
            'disks': [],
            'raid': {
                'raid': False,
                'raid_smart_command': '',
                'raid_devices': [],
                'raid_disks': [],
            },
            'smart_ids': {
                'ids': [],
                'warning_ids': []
            }
        },
        'logging': {
            'log_dir': './logs',
            'log_file': './logs/dv_smart_collector.log',
            'log_level': 'INFO',

        },
        'output': {
            'out_dir': './out',
            'out_file': './out/dv_smart_collector_output.json',
        },
        'api': {
            'post_results': False,
            'url': ''
        },
    })
    user_config = Config.from_path('./config.toml', optional=True)
    config = system_config + user_config
