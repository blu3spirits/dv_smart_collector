#!/usr/bin/env python3

from dv_smart_collector.collector_lib.configuration import CollectorConfig
from dv_smart_collector.collector_lib.exceptions import (
    SmartCollectorKeyError,
    SmartCollectorSmartctlError,
)
from dv_smart_collector.collector_lib.logger import CollectorLogger
from dv_smart_collector.collector_lib.api import DiskViewerApi
from multiprocessing import Process, Pipe
from subprocess import Popen, PIPE
from pathlib import Path
import json
import os
import signal
import socket


class SmartCollector(object):
    def __init__(self):
        self.config = CollectorConfig.config
        self.pipe = Pipe()
        self.logger = self.__set_up_logging()
        self.output_file = self.__set_up_output()
        self.smart_alert_ids = self.config['command']['smart_ids']['ids']
        self.smart_warn_ids = self.config['command']['smart_ids']['warning_ids']
        self.disks = DiskViewerApi(self.logger, self.config['api'])
        # Initialize a server to later be set by API pushes
        self.server = None
        self.logger.debug('Logging setup, program ready')

    def __set_up_output(self):
        try:
            output_dir = self.config['output']['dir']
            output_file = self.config['output']['file']
        except KeyError:
            self.logger.warning(
                        'No output definitions found, printing to stdout')

        if output_dir and output_file:
            if not Path(output_dir).exists():
                Path(output_dir).mkdir(parents=True, exist_ok=True)
                self.logger.info(f'Directory created at [{Path(output_dir)}]')
        return output_file

    def __set_up_logging(self):
        logging = self.config['logging']
        log_file = logging['log_file']
        log_dir = logging['log_dir']
        log_level = logging['log_level']
        return CollectorLogger(log_dir, log_file, log_level)

    def __create_commands(self):
        commands = list()
        raid_config = self.config['command']['raid']
        if raid_config['raid']:
            raid_command = raid_config['raid_smart_command']
            for disk in raid_config['raid_disks']:
                for device in raid_config['raid_devices']:
                    commands.append(f'{raid_command}{device} {disk}')
        smart_command = self.config['command']['smart_command']
        [commands.append(f'{smart_command} {disk}')
         for disk in self.config['command']['disks']]
        return commands

    def __check_output(self, output, command):
        try:
            status = output['smartctl']['exit_status']
        except KeyError as e:
            raise SmartCollectorKeyError(e)

        if status == 1:
            self.logger.critical(
                            'Disk unable to open, manual mode required')
            self.__exit()
            raise SmartCollectorSmartctlError(
                    'Disk was unable to open, manual mode required')
        elif status == 2:
            self.logger.critical(
                            '')
            self.__exit()
            raise SmartCollectorSmartctlError(
                f'Disk failed to open command={command} '
                f'output={output} status={status}')

    def __exit(self):
        self.logger.critical(f'Quitting with signal {signal.SIGTERM}')
        ppid = os.getppid()

        os.kill(ppid, signal.SIGTERM)

    def _run(self, command):
        child_conn = self.pipe[1]
        running_proc = Popen(args=command.split(), stdout=PIPE)
        output, err = running_proc.communicate()
        modified_output = json.loads(output)
        self.__check_output(modified_output, command)
        child_conn.send(modified_output)
        child_conn.close()

    def __run_commands(self, processes):
        parent_conn = self.pipe[0]
        outputs = list()
        for process in processes:
            process.start()
            outputs.append(parent_conn.recv())
            process.join()

        self.logger.debug('Smart commands executed successfully')
        return outputs

    def __create_processes(self):
        commands = self.__create_commands()
        self.logger.info(f'Running {len(commands)} commands. Commands={commands}')
        return [Process(target=self._run, args=(command,))
                for command in commands]

    def __process_smart_info(self, smart_attributes):
        table = smart_attributes['table']
        collected_ids = list()
        disk_status = 'OK'
        for entry in table:
            entry_id = entry['id']
            if entry_id in self.smart_alert_ids or entry_id in self.smart_warn_ids:
                collected_ids.append(entry)

        if collected_ids:
            for entry in collected_ids:
                raw_value = int(entry['raw']['value'])
                if raw_value > 0:
                    entry_id = entry['id']
                    if entry_id in self.smart_alert_ids:
                        self.logger.critical(f'Found CRITICAL smart value {entry}')
                        disk_status = 'CRIT'
                    elif entry_id in self.smart_warn_ids:
                        self.logger.warning(f'Found WARNING smart value {entry}')
                        disk_status = 'WARN'

        return disk_status

    def start_collection(self):
        processes = self.__create_processes()
        return self.__run_commands(processes)

    def parse_collections(self, data):
        formatted_data = list()
        for collection in data:
            entry = dict()
            serial_number = collection['serial_number']
            speed = collection.get('rotation_rate', 0)
            # Math out the size
            size = int(collection['user_capacity']['bytes']) // (1024**3)
            if collection['device']['type'] == 'nvme':
                status = 'OK'
            else:
                status = self.__process_smart_info(collection['ata_smart_attributes'])

            entry = {
                'serial_number': serial_number,
                'size': size,
                'speed': speed,
                'status': 'USD',
                'port': -1,
                'host': socket.gethostname(),
                'health': status,
                'server': None
            }
            self.logger.info(f'Found disk: {serial_number} '
                             f'size={size}, speed={speed}, host={socket.gethostname()}, '
                             f'status={status}')
            formatted_data.append(entry)
        return formatted_data

    def push_to_api(self, disk_data):
        serial_number = disk_data['serial_number']
        self.logger.info(f'Attempting push for disk {serial_number} '
                         f'to {self.config["api"]["url"]}')
        existing_disk = self.disks.get_disks(serial_number)
        if existing_disk:
            self.logger.info(f'Disk {serial_number} exists. Checking for differences...')
            existing_disk = existing_disk['results']
            for disk in existing_disk:
                disk_data['server'] = disk['server']
                # If the port was manually changed we don't want to change it again
                # Remove updated and created from the object
                del disk['created']
                del disk['updated']

                if disk['port'] != -1:
                    disk_data['port'] = disk['port']
                differences = disk.items() - disk_data.items()
                if differences:
                    self.logger.info(f'Updating disk {serial_number}')
                    status = self.disks.update_disk(
                                    disk_id=disk_data['serial_number'],
                                    disk_data=disk_data)
                    return status
                else:
                    self.logger.info('No updates necessary...')

        else:
            # If no server, get server
            server_name = disk_data['host']
            if not self.server:
                # Create new server and use that id
                self.server = self.disks.get_servers(server_name)
                base_object = {
                    'hostname': disk_data['host'],
                    'address': socket.gethostbyname(disk_data['host']),
                    'location': 'unset'
                }

                new_server = self.disks.create_server(base_object)
                self.server = new_server
                new_server_id = new_server['id']

                disk_data['server'] = new_server_id


            else:
                server_id = self.server['id']
                disk_data['server'] = server_id

            resp = self.disks.post_disk(disk_data=disk_data)
            return resp


def main():
    sc = SmartCollector()
    output_file = sc.output_file
    collections = sc.start_collection()
    formatted_data = sc.parse_collections(collections)
    if sc.config['api']['post_results']:
        for disk in formatted_data:
            sc.push_to_api(disk)
    else:
        sc.logger.info('API push disabled. Not posting')

    with open(output_file, 'w') as out:
        [out.write(json.dumps(collection)) for collection in collections]


if __name__ == '__main__':
    main()
