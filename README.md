# Smart Collector

This project pairs directly with my API project found [here](https://gitlab.com/blu3spirits/disk_viewer)

### Setup
1. Setup Disk Viewer
2. Clone this repo
3. Configure your config.toml
4. Run dv_smart_collector/smart_collector.py (Requires user to be able to execute smartctl with no password/sudo (root user by default)


#### Cloning
Clone this repo

```
git clone https://gitlab.com/blu3spirits/dv_smart_collector.git
```

#### Configuring `config.toml`

```
[command]
# --all Get all smart results (required)
# --json Output in json (required)
# --device=sat Your device controller (optional)
smart_command = "smartctl --all --json --device sat"
# When smart collector loads it will run these full command:
# smartctl --all --json --device sat /dev/sda
# smartctl --all --json --device sat /dev/sdb
disks = [ "/dev/sda", "/dev/sdb" ] # If left blank no smart smart command gets run
                                   # Useful for systems with RAID only setups

[command.raid]
# Enable running the raid mode command set
raid = false
# --all Get all smart results (required)
# --json Output in json (required)
# --device=megaraid, Your device controller (optional) *
# * The comma at the end of megaraid is just templating the command
raid_smart_command = "smartctl --all --json --device megaraid,"

# All (or none) of the raid devices to target
raid_devices = [ 0, 1 ]
raid_disks = [ "/dev/sda" ]
# The disk location for smartctl to run again

# When smart collector loads it will run these full command:
# smartctl --all --json --device megaraid,0 /dev/sda
# smartctl --all --json --device megaraid,1 /dev/sda

[api]
# Change to false if you don't want to report results to an API
post_results = true
# The URL to POST smart results to
url = ''

[output]
# Output directory path
dir = "./out"
# Output file path
# This script will by default dump a json output of all smart info at this location
file = "./out/smartctl_output.json"

[logging]
# log directory for this program
log_dir = "./logs"
# (DEBUG, INFO, WARNING, ERROR, CRITICAL)
log_level = 'DEBUG'
# log file for this program
log_file = "./logs/smart_collector.log"
```

#### Running smart_collector
Run smart collector as root (optional, see next section)
```
# From the top level dir of the repo
sudo python dv_smart_collector/smart_collector.py
```


#### Running as a non-root user

If you don't want to run this program as root (recommended). You will need to provision a user
with the ability to run `sudo smartct -a [device]` with no password.

Note: Make sure to replace YOURUSER with your user on your destination machines

```
# cat << '__EOF__' > /etc/sudoers.d/smartctl
YOURUSER ALL=(root) NOPASSWD: /bin/smartctl
__EOF__
```

Then add sudo to your command, for example:
```
-smart_command = "smartctl --all --json --device sat"
+smart_command = "sudo smartctl --all --json --device sat"
```

Run the file as normal:
```
python dv_smart_collector/smart_collector.py
```
